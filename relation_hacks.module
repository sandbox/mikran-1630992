<?php

/**
 * @file
 * relation_hacks API functions
 */

/**
 * Load revision that existed at the spesific time
 * 
 * @param $rid
 *   relation id
 * 
 * @param $timestamp
 *   timestamp
 */
function relation_hacks_load_revision_by_time($rid, $timestamp) {
  $query = db_select('relation_revision', 'r');
  $query->condition('r.rid', $rid)
    ->condition('r.changed', $timestamp, '<')
    ->addExpression('max(r.vid)', 'vid');
  $vid = $query->execute()->fetchCol();
  if (empty($vid)) {
    return FALSE;
  }
  return relation_load($rid, $vid);
}

/**
 * Helper function to get one endpoint from $relation of type $relation_type
 *
 * @param $relation
 *   Fully loaded relation object
 *
 * @param $entity_type
 *
 * @return
 *   entity id
 */
function relation_hacks_get_endpoint_by_type($relation, $entity_type) {
  foreach ($relation->endpoints[LANGUAGE_NONE] as $endpoint) {
    if ($endpoint['entity_type'] == $entity_type) {
      return $endpoint['entity_id'];
    }
  }
  return FALSE;
}

/**
 * When looking for connected entities of same type (eg. nodes) the other nid
 * is normally known and it's common taks to retrieve the other one. This
 * function attempts the need to write loops everytime this occurs.
 * 
 * @param $relation
 *   The relation containing at least 2 endpoints
 * @param $entity_id
 *   The entity_id of know entity
 */
function relation_hacks_get_other_endpoint($relation, $entity_id) {
  foreach ($relation->endpoints[LANGUAGE_NONE] as $endpoint) {
    if ($endpoint['entity_id'] != $entity_id) {
      return $endpoint['entity_id'];
    }
  }
}

/**
 * Return endpoint entities.
 * 
 * @param $relation
 *   Full relation object containing the endpoints
 * 
 * @param $entity_type
 *   (optional) Filter endpoints by entity type. Return all endpoint entities
 *   if empty.
 */
function relation_hacks_load_endpoint_entities($relation, $entity_type = NULL) {
  $entities = array();
  foreach ($relation->endpoints[LANGUAGE_NONE] as $endpoint) {
    if (!empty($entity_type) && $endpoint['entity_type'] != $entity_type) {
      continue;
    }
    $entity = entity_load($endpoint['entity_type'], array($endpoint['entity_id']));
    $entities[$endpoint['entity_type']][] = $endpoint['entity_id'];
  }
  if (empty($entities)) {
    return FALSE;
  }
  foreach ($entities as $entity_type => $ids) {
    $entities[$entity_type] = entity_load($entity_type, $ids);
  }
  return $entities;
}
